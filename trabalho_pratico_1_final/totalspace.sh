#!/bin/bash
 
function file_sizes()
{
 
    files_size=0
    i=0
 
    local files[0]=0
    for f in *
    do
        if [[ -f $f ]] && [[ $f =~ $NAME ]]
        then
            tmp=$(wc -c "$f" | cut -f1 -d ' ') # "$f1" prevents error if filename contains whitespaces
           
            # -d option
            if (( $MAX_DATE != 0 )); then
                if (( $(stat -c %X "$f") < $MAX_DATE )); then
                    files[i]=$tmp
                    ((i++))
                fi
                continue
            fi
 
            files[i]=$tmp
            ((i++))
        fi
    done
 
    IFS=$'\n' files=($(sort -nr <<< "${files[*]}")) # sort files in descending order
    unset IFS
 
    if (( $N_BIGGEST == -1 ))
    then
        max=${#files[@]}
    else
        max=$N_BIGGEST
    fi
    for (( i=0; i<${#files[@]} && i<max; i++ ))
    do
        files_size=$((files_size+files[i]))
    done
}
 
function dir_sizes()
{
 
    local cur_dir_size=0
    local cur_dir=$1
   
    file_sizes # Get the files in the root
    cur_dir_size=$((cur_dir_size + files_size))
 
    # Fix for Directories with whitespace
    SAVEIFS=$IFS
    IFS=$(echo -en "\n")
   
    for d in *
    do  
        if [[ -d $d ]]
        then
            cd "$d"
            dir_sizes $cur_dir/$d
            cur_dir_size=$((cur_dir_size + dir_size_to_return))
            cd ..
        fi
    done

    dir_size_to_return=$cur_dir_size
   
    tx[e]="$cur_dir_size $cur_dir"
    ((e++))
 
    IFS=$SAVEIFS
}
 
function a_files()
{
    local cur_dir=$1
 
    for f in *
    do
        if [[ -d $f ]]
        then
            cd "$f"
            a_files $cur_dir/$f
            cd ..
        elif [[ -f $f ]]
        then
            local f_size=$(wc -c "$f" | cut -f1 -d ' ')
            if [[ $f_size -gt biggest_files[$N_BIGGEST_OVERALL-1] ]] && [[ $f =~ $NAME ]]
            then
 
                # -d option
                if (( $MAX_DATE != 0 )); then
                    if (( $(stat -c %X "$f") < $MAX_DATE )); then
                        tx[N_BIGGEST_OVERALL-1]="$f_size $cur_dir/$f"
                        IFS=$'\n' tx=($(sort -nr <<<"${tx[*]}")) # sort files in descending order
                        unset IFS
                        continue
                    fi
                fi
 
                tx[N_BIGGEST_OVERALL-1]="$f_size $cur_dir/$f"
                IFS=$'\n' tx=($(sort -nr <<<"${tx[*]}")) # sort files in descending order
                unset IFS
            fi
        fi
    done
}
 
function total_space()
{
    dir=$1
 
    if [[ $N_BIGGEST_OVERALL -ne -1 ]]
    then
        for (( i=0; i<$N_BIGGEST_OVERALL; i++ ))
        do
            tx[i]=0
        done
        cd "$dir"
        a_files $dir
        cd ..
    else
        cd "$dir"
        dir_sizes $dir
        cd ..
    fi
 
    # -a(lphabetical) option
    if (($SORT_ALPHA == 1))
    then
        IFS=$'\n'
        tx=($(sort -k2 <<< "${tx[*]}")) # Sort by the second column of the array
        unset IFS
    else # Option not present (numerical sort from biggest to smallest)
        IFS=$'\n'
        tx=($(sort -nr <<< "${tx[*]}")) # Sort by numeric value
        unset IFS
    fi
 
    # -r(everse) option
    if (($SORT_REVERSE == 1))
    then
        for (( i=${#tx[@]}-1; i>=0; i-- ))
        do
            printf '%s\n' "${tx[i]}"
            continue
        done
    else
        printf '%s\n' "${tx[@]}"
    fi
}
 
NAME=".*"
N_BIGGEST=-1
MAX_DATE=0
N_BIGGEST_OVERALL=-1
SORT_ALPHA=0
SORT_REVERSE=0
 
if (($# >= 1))
then
    while getopts 'n:l:d:L:ra' c
    do
        case $c in
            n)
                NAME=$OPTARG
                ;;
            l)
                N_BIGGEST=$OPTARG
                ;;
            d)
                MAX_DATE=$(date -d "$OPTARG" +%s  2>/dev/null) # 2>/dev/null hides error output
                if (( $? == 1 )); then
                    >&2 echo Invalid date format # echo to stderr
                    exit 1
                fi
                ;;
            L)
                N_BIGGEST_OVERALL=$OPTARG
                ;;
            r)
                SORT_REVERSE=1
                ;;
            a)
                SORT_ALPHA=1
                ;;
            *)
                ;;
        esac
    done
 
    # -L and -l arguments are incompatible
    if [[ $N_BIGGEST -ne -1 && $N_BIGGEST_OVERALL -ne -1 ]]
    then
        >&2 echo Invalid Arguments -l and -L;
        exit 1;
    fi
    # # -r and -a arguments are incompatible
    # if [[ $SORT_ALPHA -eq 1 && $SORT_DESCEND -eq 1 ]]
    # then
    #     >&2 echo Invalid Arguments -r and -a;
    #     exit 1;
    # fi
    # If no directory is given, assume dir is current directory
    if [[ ${!OPTIND} == "" ]];
    then
        dir="."
    else
        dir=${!OPTIND}
    fi
 
else
    dir="."
fi
 
total_space $dir