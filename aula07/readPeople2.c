#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

typedef struct
{
    int age;
    double height;
    char name[64];
} Person;

void printPersonInfo(Person *p)
{
    printf("Person: %s, %d, %f\n", p->name, p->age, p->height);
}

int main (int argc, char *argv[])
{
    FILE *fp = NULL;
    Person p;

    int n_persons;
    Person persons[1024];

    /* Validate number of arguments */
    if(argc != 2)
    {
        printf("USAGE: %s fileName\n", argv[0]);
        return EXIT_FAILURE;
    }

    /* Open the file provided as argument */
    errno = 0;
    fp = fopen(argv[1], "rb");
    if(fp == NULL)
    {
        perror ("Error opening file!");
        return EXIT_FAILURE;
    }

    /* read all the itens of the file */
    int n_persons_o = fread(persons, sizeof(Person), 1024, fp);
    fclose(fp);

    printf("Original content: \n");
    for(int i = 0; i < n_persons_o+n_persons; i++)
    {
        printPersonInfo(&persons[i]);
    }


    //Read people data to add
    printf("\nNumber of persons to read and add: ");
    scanf("%d", &n_persons);
    for(int i = n_persons_o; i < n_persons_o+n_persons; i++)
    {
        printf("Name: ");
        scanf(" %[^\n]", p.name); // ' '(space) --> skip whitespace (one or many)
        printf("Height: ");
        scanf("%lf", &p.height);
        printf("Age: ");
        scanf("%d", &p.age);

        persons[i] = p;
    }

    printf("\nFinal Content:\n");
    for(int i = 0; i < n_persons_o+n_persons; i++)
    {
        printPersonInfo(&persons[i]);
    }


    /* Open the file provided as argument */
    errno = 0;
    fp = fopen(argv[1], "wb");
    if(fp == NULL)
    {
        perror ("Error opening file!");
        return EXIT_FAILURE;
    }
    fwrite(persons, sizeof(Person), n_persons_o+n_persons, fp);
    fclose(fp);

    return EXIT_SUCCESS;
}
