#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

typedef struct
{
    int age;
    double height;
    char name[64];
} Person;

void printPersonInfo(Person *p)
{
    printf("Person: %s, %d, %f\n", p->name, p->age, p->height);
}

int main (int argc, char *argv[])
{
    FILE *fp = NULL;
    int n_persons;

    printf("Number of persons to read: ");
    scanf("%d", &n_persons);

    Person persons[n_persons];

    /* Validate number of arguments */
    if(argc != 2)
    {
        printf("USAGE: %s fileName\n", argv[0]);
        return EXIT_FAILURE;
    }

    /* Open the file provided as argument */
    errno = 0;
    fp = fopen(argv[1], "wb");
    if(fp == NULL)
    {
        perror ("Error opening file!");
        return EXIT_FAILURE;
    }

    /* Read n_persons and write on a file */
    for(int i = 0; i < n_persons; i++)
    {
        Person p;

        printf("Name: ");
        scanf(" %[^\n]", p.name); // ' '(space) --> skip whitespace (one or many)
        printf("Height: ");
        scanf("%lf", &p.height);
        printf("Age: ");
        scanf("%d", &p.age);

        persons[i] = p;
        
        fwrite(&p, sizeof(Person), 1, fp);
    }

    fclose(fp);

    return EXIT_SUCCESS;
}
