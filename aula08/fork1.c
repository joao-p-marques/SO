#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int ret;

    printf ("Antes do fork: PID = %d, PPID = %d\n", getpid (), getppid ()); // P
    if ((ret = fork ()) < 0) { // P + F
        perror ("erro na duplicação do processo");
        return EXIT_FAILURE;
    }
    if (ret > 0) // P + F 
        sleep (1); // P
    printf ("Quem sou eu?\nApós o fork: PID = %d, PPID = %d\n", getpid (), getppid ()); // P + F

    return EXIT_SUCCESS;
}
