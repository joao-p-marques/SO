/**
 * \brief The server
 * \details This server converts strings to upper case
 * \autores : Artur Pereira e José Luís Oliveira
 */

#include "comm.h"
#include "message.h"

#include <ctype.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>

#define MSG_KEY 0x1111UL

static int msgQueue;

static struct cont
{
    long client;
    MESSAGE msg;
} container;

void open_comm() {
    if ((msgQueue = msg_create (MSG_KEY)) == -1) { 
        if (errno == EEXIST) { 
            if ((msgQueue = msg_connect (MSG_KEY)) == -1) { 
                perror("Fail creating message queue");
                exit(EXIT_FAILURE);
            }
        }
        else { 
            perror("Fail creating message queue");
            exit(EXIT_FAILURE);
        }
    }
}

void receive_msg(MESSAGE * msg) {
    /* get response message */
    if (msg_receive (msgQueue, &container, sizeof(MESSAGE), 0L) == -1) { 
        perror ("Fail receiving message from message queue");
        exit(EXIT_FAILURE);
    }

    /* copy message into caller area */
    *msg = container.msg;
    //printf("[Client \'%ld\'] message received.\n", container.client);
}

void close_comm() {
    if (msg_destroy (msgQueue) == -1) { 
        perror ("Fail destroying message queue");
        exit(EXIT_FAILURE);
    }
}


int main(void)
{
    /* Creating communication channel */
    open_comm();

    /* the service */
    MESSAGE msg;
    while(1) {
        /* receive message from talker */
        receive_msg(&msg);

        /* printing received message */
        printf("Message received: %s", msg.data);
    }

    close_comm();
    return 0;
}
