/**
 * \brief The server
 * \details This server converts strings to upper case
 * \autores : Artur Pereira e José Luís Oliveira
 */

#include "comm.h"
#include "message.h"

#include <ctype.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <string.h>

#define MSG_KEY 0x1111UL

static int msgQueue;

static struct cont
{
    long client;
    MESSAGE msg;
} container;

void open_comm() {
    if ((msgQueue = msg_create (MSG_KEY)) == -1) { 
        if (errno == EEXIST) { 
            if ((msgQueue = msg_connect (MSG_KEY)) == -1) { 
                perror("Fail creating message queue");
                exit(EXIT_FAILURE);
            }
        }
        else { 
            perror("Fail creating message queue");
            exit(EXIT_FAILURE);
        }
    }
}

void send_msg(MESSAGE * msg) {
    /* copy message into container */
    container.client = getpid();
    container.msg = *msg;

    /* send message */
    if (msg_send_nb (msgQueue, &container, sizeof(MESSAGE)) == -1) { 
        perror ("Fail sending message to message queue");
        exit(EXIT_FAILURE);
    }
}

void close_comm() {
    if (msg_destroy (msgQueue) == -1) { 
        perror ("Fail destroying message queue");
        exit(EXIT_FAILURE);
    }
}


int main(void)
{
    /* Creating communication channel */
    open_comm();

    /* the service */
    MESSAGE msg;
    while(1) {
        /* asking user for a message */
        printf("\n[client \'%d\'] Message to be sent: ", getpid());
        fgets(msg.data, MSG_MAX, stdin);
        msg.size = strlen(msg.data)+1;

        send_msg(&msg);

        printf("\nMessage sent: %s", msg.data);
    }

    close_comm();
    return 0;
}