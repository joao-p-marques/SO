#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

/* SUGESTÂO: utilize as páginas do manual para conhecer mais sobre as funções usadas:
  man opendir
  man readdir
*/

void listDir(char dirname[])
{
    DIR *dp; //pointer to the directory
    struct dirent *dent;

    dp = opendir(dirname); //pointer to the directory
    if(dp == NULL)
        return;

    dent = readdir(dp); //struct representing each element in the directory
    while(dent!=NULL) 
    {
        if(dent->d_name[0] != '.') // do not list hidden dirs/files
        {
            if(dent->d_type == (unsigned char)'DT_DIR') //if element is a directory
            {
                printf("d ");
            }
            else if(dent->d_type == (unsigned char)'DT_REG') //if element is a regular file
            {
                printf("  ");
            }
            printf("%s/%s\n",dirname,dent->d_name);
        }

        dent = readdir(dp);
    }
}

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        fprintf(stderr,"Usage: %s base_directory\n",argv[0]);
        return EXIT_FAILURE;
    }

    listDir(argv[1]);
    
    return EXIT_SUCCESS;
}


