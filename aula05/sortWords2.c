#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compare_p_str(const void *ps1,const void *ps2)
{
    char *p1 = *(char **) ps1;
    char *p2 = *(char **) ps2;
    return strcmp(p1, p2);
}

int main(int argc, char **argvg)
{
    char word[100];
    char **words;
    int c=0;

    printf("Word ('end' to finish): ");
    scanf("%s", word);
    words = (char **) malloc(1 * sizeof(char *));
    words[c] = (char *) malloc((strlen(word)+1) * sizeof(char));
    strcpy(words[c], word);
    c++; //c=1

    printf("Word ('end' to finish): ");
    scanf("%s", word);
    while(strcmp(word, "end") != 0)
    {
        c++;
        words = (char **) realloc(words, c * sizeof(char *));
        words[c-1] = strdup(word);
        printf("Word ('end' to finish): ");
        scanf("%s", word);
    }

    // for(int i=0; i<c; i++){
    //     printf("%s\n", words[i]);
    // }

    qsort(words, c, sizeof(char *), &compare_p_str);

    for(int i=0; i<c; i++){
        puts(words[i]);
    }
}